# Innovecs Todo Assignment

This is a todo application for Innovecs HR team. 

### .env
```
REACT_APP_BASE_URL_DEV=
REACT_APP_BASE_URL_PROD=
```
PROD URL must be present

### Architecture
The architecture of the application is extremely simple. There are 4 major components

####TodoHolder
This component is responsible for rendering the array given by ```App.js``` in a stack of ```SingleTodo```'s. The component also provides information to the user if there are currently no todos available to show
####SingleTodo
As the name suggests the component renders a single todo inside a ```Box```

####InputColumn
This is responsible for rendering the ```Form```, ```Input``` and ```Button``` components for adding todos. The backend API calls are made through this component as well, and it also responsible for error handling (showing toasts)
####App
The app component is where all the UI elements come together. It also fetches the todos from the backend when the component mounts. The component is able to refetch the todos when ```InputColumn``` calls its ```refetch``` prop 
### Deployment
There were many problems with the deployment of the both application (backend, and front-end). I was able to dockerize both applications and create a CI/CD pipeline that was tasked with testing, building, and deploying to the server. <br/>
<br/>
However, I was not able to get this pipeline to work concurrently. Whenever I would commit to main on the front-end application and verified that it is working in production, the backend application that was working before would stop working.
<br/><br/>
Whatever application had the latest pipeline run would be the only application that works in production, the other one would just stop working.
<br/><br/>
After hours and hours of trying to fix the issue, I decided to manually pull the repos from gitlab and run the applications in docker
<br/><br/>
Both applications have their own nginx conf, I realized that this was not really necessary as the host server has nginx set up that acts as a reverse proxy to these applications. I implemented this type of architecture as this way it was really easy for me to get the SSL certificates for the two subdomains
<br/><br/>
```Backend https://innovecsapi.gasimzade.az/api/v1``` 
<br/>
```Frontend https://innovecs.gasimzade.az``` 

### Scripts
You can build the application as such
<br/>
```docker build -t frontend .```
<br/>
<br/>
And run it as such
<br/>
```docker run -p ${desired_port}:80 -d frontend```