FROM node:14 AS builder
WORKDIR /front-end
COPY . /front-end

RUN yarn install
RUN yarn build

FROM nginx:1.16.0-alpine
COPY --from=builder /front-end/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
