import React from "react";
import {shallow} from "enzyme";
import InputColumn from "./InputColumn";
import {Formik, Form, Field} from "formik";
import {Box} from "@chakra-ui/react";

describe('<InputColumn />', () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<InputColumn />);
    });

    it('should have box', () => {
        expect(wrapper.find(Box)).toHaveLength(1);
    });
});