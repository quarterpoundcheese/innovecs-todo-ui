import React from "react";
import {shallow} from "enzyme";
import SingleTodo from "./SingleTodo";
import {Box, Flex, Text} from "@chakra-ui/react";
import TodoHolder from "./TodoHolder";

describe('<TodoHolder />', () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<TodoHolder />);
    });

    it('should have box', () => {
        expect(wrapper.find(Box)).toHaveLength(1);
    });
    it('should have 3 Single Todos', () => {
        expect(wrapper.find(SingleTodo)).toHaveLength(3);
    });

});