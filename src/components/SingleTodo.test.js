import React from "react";
import {shallow} from "enzyme";
import SingleTodo from "./SingleTodo";
import {Box, Flex, Text} from "@chakra-ui/react";

describe('<SingleTodo />', () => {

    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<SingleTodo todo={{}} />);
    });

    it('should have box', () => {
        expect(wrapper.find(Box)).toHaveLength(1);
    });

    it('should have flex', () => {
        expect(wrapper.find(Flex)).toHaveLength(1);
    });

    it('should have texts', () => {
        expect(wrapper.find(Text)).toHaveLength(2);
    });

});