import React from "react";
import {Box, Stack, Text} from "@chakra-ui/react";
import SingleTodo from "./SingleTodo";

export default function TodoHolder({todos = [{}, {}, {}]}) {
    return (
        <Box>
            {todos.length === 0 ? <Text textAlign={"center"}>No todos to show</Text> : <Stack>
                {
                    todos.map((t, k) => <SingleTodo todo={t} key={k}/>)
                }
            </Stack>
            }
        </Box>
    )
}