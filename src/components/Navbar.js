import React from "react";
import {Box, Flex, Heading, Spacer, Text} from "@chakra-ui/react";
import ResponsiveBox from "./ResponsiveBox";

export default function Navbar() {
    return (
        <Box py={3} boxShadow={"lg"} bgColor={"gray.200"}>
            <ResponsiveBox mx={'auto'}>
                <Flex alignItems={"center"}>
                    <Heading>Innovecs Todo App</Heading>
                    <Spacer />
                    <Text>Ali Gasimzade</Text>
                </Flex>
            </ResponsiveBox>
        </Box>
    )
}