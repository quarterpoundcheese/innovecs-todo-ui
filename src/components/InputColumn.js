import React from "react";
import {Box, Button, FormControl, FormErrorMessage, Grid, Input, useToast} from "@chakra-ui/react";
import {Formik, Form, Field} from "formik"
import * as Yup from 'yup'
import {AddTodo} from "../adapters/api.adapter";

export default function InputColumn(
    {
        reFetch = () => {
        }
    }) {
    const toast = useToast();


    // eslint-disable-next-line no-control-regex

    const todoSchema = Yup.object().shape({
        todo_title: Yup.string()
            .min(3, "Title is too short")
            .max(100, "Title is too long")
            .required("Title is required")
            .matches(/^[\w ]+$/, "Must be ascii only")
    })

    const addTodo = async (todo) => {
        try {
            await AddTodo(todo.todo_title);
        } catch (e) {
            throw e;
        }
    }

    return (
        <Box>
            <Formik
                initialValues={{todo_title: ""}}
                validationSchema={todoSchema}
                onSubmit={(values, {resetForm, setSubmitting}) => {
                    addTodo(values).then(() => {
                        toast({
                            title: "Added todo!",
                            status: "success",
                            duration: 2000,
                            isClosable: true,
                        })
                        reFetch();
                        resetForm()
                    }).catch((e) => {
                        console.log(e);
                        toast({
                            title: "Something went wrong :(",
                            status: "error",
                            duration: 9000,
                            isClosable: true,
                        })
                    }).finally(() => setSubmitting(false))
                }}
            >
                {
                    (props) => (
                        <Form>
                            <Grid gridGap={3} gridTemplateColumns={"3fr 1fr"}>
                                <Field name={"todo_title"}>
                                    {
                                        ({field, form}) => (
                                            <FormControl isRequired
                                                         isInvalid={form.errors.todo_title && form.touched.todo_title}>
                                                <Input {...field} placeholder={"Enter a title for your todo"}/>
                                                <FormErrorMessage>{form.errors.todo_title}</FormErrorMessage>
                                            </FormControl>
                                        )
                                    }
                                </Field>
                                <Button
                                    type={"submit"}
                                    isLoading={props.isSubmitting}
                                >
                                    Add todo
                                </Button>
                            </Grid>
                        </Form>
                    )
                }
            </Formik>
        </Box>
    )
}