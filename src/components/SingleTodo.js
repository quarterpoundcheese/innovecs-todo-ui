import React from "react";
import {Box, Flex, Spacer, Text} from "@chakra-ui/react";
import dayjs from "dayjs";

export default function SingleTodo({todo: {todo_title = "Salam", CreatedAt = "10/10/2021"}}) {
    return (
        <Box px={3} py={4} borderRadius={12} border={"1px solid #ccc"}>
            <Flex alignItems={"center"}>
                <Text>{todo_title}</Text>
                <Spacer/>
                <Text>{dayjs(CreatedAt).format("YYYY-MM-DD HH:mm")}</Text>
            </Flex>
        </Box>
    )
}
