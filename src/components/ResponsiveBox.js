import React from "react";
import {Box} from "@chakra-ui/react";

export default function ResponsiveBox(props) {
    return (
        <Box maxW={"1920px"} {...props} w={['90%', '85%', '70%', '65%', '60%']}>
            {props.children}
        </Box>
    )
}