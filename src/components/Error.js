import React from "react";
import {Text} from "@chakra-ui/react";

export default function Error() {
    return (
        <Text textAlign={"center"}>Something went wrong</Text>
    )
}