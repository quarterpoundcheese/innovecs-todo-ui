import axios_api_instance from "../config/api.config";

export const AddTodo = async (title) => {
    try {
        const result = await axios_api_instance.post("/todo/", {todo_title: title});
        return result.data
    } catch (e) {
        throw e.response
    }
}

export const GetTodos = async () => {
    try {
        const result = await axios_api_instance.get("/todo/");
        return result.data
    } catch (e) {
        throw e.response
    }
}

