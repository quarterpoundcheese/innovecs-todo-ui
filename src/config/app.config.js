let API_CONFIG = {
    production: {
        api: {
            baseURL: process.env.REACT_APP_BASE_URL_PROD,
        },
    },
    development: {
        api: {
                baseURL: process.env.REACT_APP_BASE_URL_DEV,
        },
    },
    test: {
        api: {
            baseURL: process.env.REACT_APP_BASE_URL_DEV,
        },
    },
};
export default API_CONFIG[process.env.NODE_ENV];