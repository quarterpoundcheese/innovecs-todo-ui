import axios from "axios";
import appConfig from './app.config';

const axios_api_instance = axios.create({
    baseURL: appConfig.api.baseURL,
});

export default axios_api_instance