import React from "react";
import {Box, useToast} from "@chakra-ui/react";
import Navbar from "./components/Navbar";
import InputColumn from "./components/InputColumn";
import ResponsiveBox from "./components/ResponsiveBox";
import TodoHolder from "./components/TodoHolder";
import {useEffect, useState} from "react";
import Loading from "./components/Loading";
import {GetTodos} from "./adapters/api.adapter";
import Error from "./components/Error";

function App() {
    const [loading, setLoading] = useState(true);
    const [update, setUpdate] = useState(false);
    const [error, setError] = useState(false);
    const [todos, setTodos] = useState();
    const toast = useToast();

    useEffect(() => {
        GetTodos().then((data) => setTodos(data)).catch((e) => {
            console.log(e)
            setError(true);
            toast({
                title: "Oops, something went wrong :(",
                description: "Something went wrong while fetching data, try again later",
                status: "error",
                duration: 9000,
                isClosable: true,
            })
        }).finally(() => setLoading(false))
        // eslint-disable-next-line
    }, [update])

    return (
        <Box>
            <Navbar />
            <ResponsiveBox mx={'auto'} mt={8}>
                <InputColumn reFetch={() => setUpdate(!update)} />
            </ResponsiveBox>
            <ResponsiveBox mt={8} mx={'auto'}>
                {loading ? <Loading /> : error ? <Error /> : <TodoHolder todos={todos} />}
            </ResponsiveBox>
        </Box>
    );
}

export default App;
